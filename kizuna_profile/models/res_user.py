# -*- coding: utf-8 -*-
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import pytz

class Users(models.Model):
    _inherit = "res.users"
    
    def _default_groups(self):
        return False
    
    def _default_role(self):
        _default_role = self.env.ref('general_base.profile_demo_user', raise_if_not_found=False)
        return _default_role
    
    groups_id = fields.Many2many('res.groups', 'res_groups_users_rel', 'uid', 'gid', string='Groups', default=_default_groups, copy=False)
    profile_ids = fields.Many2many('profile', 'res_users_profiles_rel', 'user_id', 'profile', string='Roles', help='The roles of the user. The admin should have no roles.')
     
    parent_id = fields.Many2one(related='partner_id.parent_id', inherited=True) 
    
    @api.model
    def create(self, values):         
        values['email'] = values['login']
        if 'company_id' in values:
            company = self.env['res.company'].browse(values['company_id'])
            if company and company and company.partner_id:
                values['parent_id'] = company and company.partner_id.id
        users = super(Users, self).create(values)
        if 'profile_ids' in values:
            self.env['profile'].update_groups(user_ids=users)
        return users
    
    @api.multi
    def write(self, values):   
        if 'login' in values:
            values['email'] = values['login']
        if 'company_id' in values:
            company = self.env['res.company'].browse(values['company_id'])
            if company and company and company.partner_id:
                values['parent_id'] = company and company.partner_id.id
        if values.get('active') == False:
            raise UserError(_("You cannot deactivate the user you're currently logged in as."))
        
        res = super(Users, self).write(values)
        if 'profile_ids' in values:
            self.env['profile'].update_groups(user_ids=self)
        return res
    
    @api.multi
    def unlink(self):
        main_users = self._get_main_user()
        if any(self.filtered(lambda x: x.id in main_users.ids)):
            raise UserError(_('You can not remove the main user as it is used internally for resources created by System (updates, module installation, ...)'))
        return super(Users, self).unlink()
    
    @api.model
    def _get_main_user(self):
        user_main = self.env.ref('base.user_root')
        user_main |= self.env.ref('base.public_user')
        user_main |= self.env.ref('base.default_user')
        return user_main
    
    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if context.get('hide_main_user'):
            users_main = self.env['res.users']._get_main_user()
            if users_main:
                args += [('id','not in',users_main.ids)] 
        return super(Users, self).search(args, offset, limit, order, count=count)
                                          
    
    @api.multi
    def update_profile(self):
        self.env['profile'].update_groups(user_ids=self)   
        return True
    
    @api.multi
    def toggle_active(self):
        self.ensure_one()
        self.active = not self.active
        self.partner_id.active = self.active
        return True
            
    def _get_astimezone_datetime(self, datetime_formart=None):
        if not datetime_formart:
            return
        datetime_formart = pytz.utc.localize(fields.Datetime.from_string(datetime_formart))
        datetime_formart = datetime_formart.astimezone(pytz.timezone(self._context.get('tz') or 'UTC'))
        return datetime_formart
    
    @api.model
    def _convert_date_datetime_to_utc(self, value, datetime_format=False):
        ''': Chuc nang dung de nhan gia tri date, datetime ve timezone UTC de luu tru tren postgres '''
        if not value:
            return False
        
        tz_utc = pytz.utc
        tz_user = pytz.timezone(self._context.get('tz') or self.env.user.tz or 'UTC')
        
        server_formart = DEFAULT_SERVER_DATE_FORMAT
        separator = [' ', '/', '-']
        date_format = ['%mr%dr%Y', '%dr%mr%Y', '%Yr%mr%d', '%Yr%dr%m']
        date_patterns = [pattern.replace('r', sep) for sep in separator for pattern in date_format]
        date_patterns.extend([p.replace('Y', 'y') for p in date_patterns])
        if datetime_format:
            server_formart = DEFAULT_SERVER_DATETIME_FORMAT
            date_patterns = [pattern + ' %H:%M:%S' for pattern in date_patterns]

        def check_patterns(patterns, value):
            for pattern in patterns:
                match = True
                try:
                    datetime.strptime(value, pattern)
                except Exception as e:
                    continue
                if match:
                    return pattern
            return False
        
        user_formart = check_patterns(date_patterns, value)
        value_formart = tz_user.localize(datetime.strptime(value, user_formart)).astimezone(tz_utc).strftime(server_formart)
        return value_formart
