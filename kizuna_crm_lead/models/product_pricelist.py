# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

array_pricing_term = [
    ('quarterly','Quarterly: Apply new unit price on 1st January of each year '),
    ('yearly','Yearly: Apply new unit price on 1st January of each year '),
    ('3_5_year_prepaid','3-year prepaid/ 5-year prepaid: Fixed during rent time')
]
array_pricing_unit = [
    ('vnd','VND/m2/month'),
    ('usd','USD/m2/month')
]

class productPricelist(models.Model):
    _inherit = "product.pricelist"

    pricing_term = fields.Selection(array_pricing_term,string='Pricing Term',default='quarterly')
    pricing_unit = fields.Selection(array_pricing_unit,string='Pricing Unit',default='vnd')

