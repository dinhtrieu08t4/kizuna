# -*- coding: utf-8 -*-
{
    'name': 'Kizuna - Profile',
    'category': 'Base',
    'author': 'Pham Dang Vinh<vinhdangpham@gmail.com>',
    'depends': ['base'],
    'data': [
        "security/ir.model.access.csv",
        "views/profile_view.xml",
        "wizard/update_profile_view.xml",
            ],
    'installable': True,
    'application': False,
    'auto_install': True,
}
