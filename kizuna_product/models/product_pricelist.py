# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

class KizunaPricelist(models.Model):
    _inherit = "product.pricelist"
    
    @api.multi
    def name_get(self):
        return [(pricelist.id, pricelist.name) for pricelist in self]