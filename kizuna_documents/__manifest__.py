{
    'name': 'Kizuna Documents',
    'version': '12.0.0.0',
    'summary': 'Kizuna Documents',
    'description': 'Module of kizuna customize module documents',
    'category': 'Sales',
    'author': 'kizuna',
    'depends': ['documents','crm','sales_team','sale'],
    'data': [
            'views/document_view.xml',
            'views/res_partner_view.xml',
    ],
    'demo': [''],
    'installable': True,
    'auto_install': False,
}