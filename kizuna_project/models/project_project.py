# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class Project(models.Model):
    _inherit = "project.project"
    
    opportunity_id = fields.Many2one('crm.lead', string='Opportunity ID', required=True, domain="['&',('type', '=', 'opportunity'), ('user_id', '=', uid)]")
    
    
    @api.onchange('opportunity_id')
    def _onchange_opportunity(self):
        self.name = self.opportunity_id.id
        self.user_id = self.opportunity_id.user_id
        self.partner_id = self.opportunity_id.partner_id
        return {}
