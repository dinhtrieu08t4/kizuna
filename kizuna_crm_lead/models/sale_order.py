# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

array_pricing_term = [
    ('quarterly','Quarterly: Apply new unit price on 1st January of each year '),
    ('yearly','Yearly: Apply new unit price on 1st January of each year '),
    ('3_5_year_prepaid','3-year prepaid/ 5-year prepaid: Fixed during rent time')
]
array_pricing_unit = [
    ('vnd','VND/m2/month'),
    ('usd','USD/m2/month')
]

class saleOrder(models.Model):
    _inherit = "sale.order"

    pricing_term = fields.Selection(array_pricing_term, related="pricelist_id.pricing_term", string='Pricing Term', default='quarterly')
    pricing_unit = fields.Selection(array_pricing_unit, related="pricelist_id.pricing_unit",string='Pricing Unit', default='vnd')


class saleOrderLine(models.Model):
    _inherit = "sale.order.line"

    # def _get_domain_product(self):
    #     domain = [('sale_ok','=',True)]
    #     print (self._context)
    #     if self._context.get('validity_date'):
    #         print (self._context.get('validity_date'))
    #         # product_tmpl_ids = self.env['product.template'].search([('seller_ids.name','in',[self._context.get('partner_id')])])
    #         # domain = [('product_tmpl_id','in',product_tmpl_ids.ids),('sale_ok','=',True)]
    #     return domain
    validity_date = fields.Date(related='order_id.validity_date',string='Validity Date')
    # product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True),('product_tmpl_id.contract_end_date','<',validity_date)], change_default=True, ondelete='restrict')

