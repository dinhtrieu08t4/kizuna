# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, SUPERUSER_ID, modules

class resPartner(models.Model):
    _inherit = 'res.partner'

    document_ids = fields.One2many('ir.attachment', 'res_partner_id', string='Documents')
    document_count = fields.Integer(string='Document Count', compute='_compute_document_count')

    @api.multi
    def _compute_document_count(self):
        for doc in self:
            documents = doc.document_ids.mapped('res_partner_id').filtered(lambda x: x.active == True)
            doc.document_count = len(documents.ids)