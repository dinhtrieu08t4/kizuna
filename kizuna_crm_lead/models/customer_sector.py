# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class crmSector(models.Model):
    _name = "crm.customer.sector"
    _description = "CRM Customer Sector"
    name = fields.Char(string='Customer Sector Name')
    active = fields.Boolean(string='Active', default=False)
