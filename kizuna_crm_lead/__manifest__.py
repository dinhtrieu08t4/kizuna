{
    'name': 'Kizuna CRM Lead',
    'version': '12.0.0.0',
    'summary': 'Kizuna CRM',
    'description': 'Module of kizuna customize module CRM Odoo',
    'category': 'Sales',
    'author': 'kizuna',
    'depends': ['crm','sales_team','sale','kizuna_documents'],
    'data': [
        #security
        'security/security.xml',
        'security/ir.model.access.csv',
        #view
        'views/crm_lead_view.xml',
        'views/sale_order_view.xml',
        'views/product_pricelist_view.xml',
        #wizard
        'wizard/crm_lead_to_opportunity.xml'
    ],
    'demo': [''],
    'installable': True,
    'auto_install': False,
}