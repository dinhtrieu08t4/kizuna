{
    'name': 'Kizuna Product',
    'version': '12.0.0.0',
    'summary': 'Kizuna Product',
    'description': 'Module of kizuna customize module product Odoo',
    'category': 'Product',
    'author': 'kizuna',
    'depends': ['product'],
    'data': [
        'security/ir.model.access.csv',
        'views/product_template_view.xml',
    ],
    'demo': [
        'data/kizuna_zone.xml'
        ],
    'installable': True,
    'auto_install': False,
}