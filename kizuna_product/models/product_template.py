# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

class WorkshopZone(models.Model):
    _name = "product.template.zone"
    _description = "Workshop Zone"

    name = fields.Char(string='Zone')
    block_list = fields.One2many('product.template.zone.block', 'zone_id', string='Kizuna Block')

class WorkshopZone(models.Model):
    _name = "product.template.zone.block"
    _description = "Workshop Block"

    name = fields.Char(string='Block')
    zone_id = fields.Many2one('product.template.zone', string='Kizuna Zone')

class Workshop(models.Model):
    _inherit = 'product.template'

    zone_id = fields.Many2one('product.template.zone', string='Kizuna Zone', required=True)
    block_id = fields.Many2one('product.template.zone.block', string='Block Name', domain="[('zone_id','=', zone_id)]", required=True)
    total_area = fields.Char(string='Total Area', required=True)
    canopys_area = fields.Char(string='Canopy\'s Area', required=True)
    mfloors_area = fields.Char(string='M Floor\'s Area', required=True)
    load_capacity = fields.Char(string='Load Capacity', required=True)
    electric_capacity = fields.Char(string='Electric Capacity', required=True)
    other_notes = fields.Text(string='Other notes', required=True)
    #contract_end_date = fields.Date(string='Contract End Date')
    # onchange handler
    @api.onchange('zone_id', 'block_id')
    def _onchange_price(self):
        # set auto-changing field
        if self.zone_id:
            if self.block_id:
                self.name = self.zone_id.name + '_' + self.block_id.name
            else:
                self.name = self.zone_id.name
        # Can optionally return a warning and domains
        return {
        }

