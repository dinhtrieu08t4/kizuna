# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

customer_array_source = [
    ('broker', 'Brokers'),
    ('Ad', 'Advertisement'),
    ('bank', 'Banks'),
    ('partner', 'Partners'),
    ('word_of_mouth', 'Word of Mouth'),
    ('website', 'Website'),
    ('support', 'Supporting Organization'),
    ('news', 'Newsletter'),
    ('semi', 'Seminars'),
    ('billboard', 'Billboard'),
]
class Lead(models.Model):
    _inherit = "crm.lead"

    lead_type = fields.Selection([('tenant', 'Tenant'), ('broker', 'Broker'), ('target', 'Target'),], string='Leads', default='tenant')
    customer_offical_name = fields.Char(string='Customer Official Name')
    customer_sector = fields.Many2one('crm.customer.sector', string='Customer Sector')
    supervisor_user_id = fields.Many2one('res.users', string='Sales Supervisor')
    document_ids = fields.One2many('ir.attachment','crm_lead_id',string='Documents')
    document_count = fields.Integer(string='Document Count',compute='_compute_document_count')

    @api.multi
    def _compute_document_count(self):
        for doc in self:
            documents = doc.document_ids.mapped('crm_lead_id').filtered(lambda x: x.active == True )
            doc.document_count = len(documents.ids)

    @api.multi
    def name_get(self):
        result = []
        
        for record in self:
            result.append((record.id, "%s - %s" % (record.id, record.name)))

        return result


