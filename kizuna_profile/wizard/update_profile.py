# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class UpdateProfile(models.TransientModel):
    _name = "update.profile"
    _description = "Update Profile"
    
    is_all = fields.Boolean(string='Update for All', default=False)
    user_ids = fields.Many2many('res.users', string='Users')
    
    @api.model
    def default_get(self, fields):
        res = super(UpdateProfile, self).default_get(fields)
        active_ids = self.env.context.get('active_ids')
        if self.env.context.get('active_model') == 'res.users' and active_ids:
            res['user_ids'] = active_ids
        return res
    
    @api.multi    
    def update_profile(self):
        user_ids = self.user_ids 
        if self.is_all:
            user_ids = self.env['res.users'].search([])
        self.env['profile'].update_groups(user_ids)
        return {'type': 'ir.actions.act_window_close'}
    
