# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.tools.translate import _

class Profile(models.Model):
    _name = "profile"
    _description = "Roles"
    
    @api.model
    def _get_default_category(self):
        category = self.env.ref('base.module_category_extra', raise_if_not_found=False)
        return category
    
    name = fields.Char('Reference', required=True, default='New', copy=False)
    code = fields.Char('Code', default='/', copy=False)
    user_id = fields.Many2one('res.users', string='Based on user', readonly=False, copy=False)
    users_ids = fields.Many2many('res.users', 'res_users_profiles_rel', 'profile', 'user_id', string='Users', required=False, readonly=False)
    groups_ids = fields.Many2many('res.groups', 'profiles_groups_rel', 'profile_id', 'group_id', string='Groups', required=False, readonly=False)
    description = fields.Text('Description')
    category_id = fields.Many2one('ir.module.category', string='Application', index=True, default=_get_default_category)

    def name_get(self):
        ret_list = []
        for profile in self:
            name = profile.name
            if profile.category_id:
                name = '%s/ %s' % (profile.category_id.name, profile.name)
            ret_list.append((profile.id, name))
        return ret_list
    
    @api.model
    def create(self, values):
        values['code'] = self.env['ir.sequence'].next_by_code('profile.code')
        return super(Profile, self).create(values) 
    
    @api.multi
    def unlink(self):
        users = self.mapped('users_ids')
        res = super(Profile, self).unlink()
        self.update_groups(user_ids=users)
        return res
    
    @api.multi
    def write(self, values):
        users = self.env['res.users']
        for role in self:
            if 'user_id' in values:
                values['groups_ids'] = [(4, x.id) for x in role.user_id.mapped('groups_id')]
                users = role.mapped('users_ids')
            elif 'groups_ids' in values:
                users = role.mapped('users_ids')
        res = super(Profile, self).write(values)
        self.update_groups(user_ids=users)
        return res
    
    @api.multi
    def load_groups(self):
        if self.user_id:
            self.groups_ids = [(4, x.id) for x in self.user_id.mapped('groups_id')]
    
    @api.multi
    def update_groups(self, user_ids=None):
        if not user_ids:
            return
        main_users = self.env['res.users']._get_main_user()
        for user in user_ids.filtered(lambda x: x.id not in main_users.ids):
            user.write({'groups_id': [(5, 0, 0)]})
            groups = self.env['res.groups']
            if user.profile_ids:
                groups = user.mapped('profile_ids').mapped('groups_ids')
                groups |= groups.mapped('implied_ids')
                groups = list(set(groups)) 
            if groups:
                user.write({'groups_id': [(6, 0, [x.id for x in groups])]})
        return True
    
    @api.onchange('user_id')
    def onchange_user(self):
        if not self.user_id:
            return
        self.groups_ids = [(4, x.id) for x in self.user_id.mapped('groups_id')]
    
class ResGroups(models.Model):
    _inherit = "res.groups"
    
    profiles_ids = fields.Many2many('profile', 'profiles_groups_rel', 'group_id', 'profile_id', string='Access Roles')
    
