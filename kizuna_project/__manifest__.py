{
    'name': 'Kizuna Project',
    'version': '12.0.0.0',
    'summary': 'Kizuna Project',
    'description': 'Module of kizuna customize module Project Odoo',
    'category': 'Project',
    'author': 'kizuna',
    'depends': ['project', 'kizuna_crm_lead'],
    'data': [
        #security
        #'security/ir.model.access.csv',
        #view
        'views/project_views.xml',
        #wizard
        #'wizard/project_to_opportunity.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}